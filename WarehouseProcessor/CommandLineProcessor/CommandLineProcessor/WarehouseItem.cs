﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandLineProcessor
{
    public class WarehouseItem
    {
        public List<string> categories { get; set; }
        public string twitter { get; set; }
        public string title { get; set; }

        public override string ToString()
        {
            return "Name: "+title+"; Categories: "+ String.Join(", ", categories) + "; Twitter: "+twitter;
        }
    }

    public class Products
    {
        public List<WarehouseItem> products { get; set; }
        
    }
}
