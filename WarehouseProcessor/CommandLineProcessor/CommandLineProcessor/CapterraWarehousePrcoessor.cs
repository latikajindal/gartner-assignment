﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YamlDotNet.Serialization;

namespace CommandLineProcessor
{
    public class CapterraWarehousePrcoessor : IWarehouseProcessor
    {
        public void Process(string filePath)
        {
            try
            {
                string text = System.IO.File.ReadAllText(filePath);

                List<Hashtable> data = YamlParser.GetInstance().Parse(text);

                if (data != null)
                {
                    List<WarehouseItem> items = new List<WarehouseItem>();
                    foreach (var item in data)
                    {

                        List<string> categories = new List<string>
                        {
                            (string)item["tags"]
                        };

                        WarehouseItem warehouseItem = new WarehouseItem();
                        warehouseItem.title = (string)item["name"];

                        warehouseItem.categories = categories;
                        warehouseItem.twitter = (string)item["twitter"];

                        items.Add(warehouseItem);

                        Console.WriteLine("importing:" + warehouseItem.ToString());
                    }
                    //DB Provider can be called here now if we want to save in DB
                    Console.ReadLine();
                }
            }

            catch (Exception ex)
            {
                Console.WriteLine("Exception while reading the file" + ex);
                return;
            }
        }
    }
}
