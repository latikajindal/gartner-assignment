﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandLineProcessor
{
    public enum ParserType
    {
        // following are the different file formats
        YAMl ,
        JSON  
    }

    public interface IParser
    {
        ParserType getParserType();

        void parse(String text);
    }
}
