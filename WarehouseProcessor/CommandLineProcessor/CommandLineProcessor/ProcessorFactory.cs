﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandLineProcessor
{
    public class ProcessorFactory
    {
        public static IWarehouseProcessor getInstance( WarehouseProvider warehouseProvider) {

            switch (warehouseProvider) {

                case WarehouseProvider.capterra:
                    return new CapterraWarehousePrcoessor();

                case WarehouseProvider.softwareadvice:

                    return new SoftwareadviceWarehouseProcessor();

                default:

                    return null;
            }

        }
    }
}
