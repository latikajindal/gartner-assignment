﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YamlDotNet.Serialization;

namespace CommandLineProcessor
{
    public class YamlParser
    {

        private static YamlParser yamlParser = null;

        public static YamlParser GetInstance()
        {
            if (yamlParser == null)
            {
                yamlParser = new YamlParser();
            }
            return yamlParser;

        }

        public List<Hashtable> Parse(string text)
        {
            var deserializer = new Deserializer();

            try
            {
                return deserializer.Deserialize<List<Hashtable>>(new StringReader(text));
            }
            catch (Exception)
            {
                Console.WriteLine("Unable to parse text");
                return null;
            }
        }
    }
}
