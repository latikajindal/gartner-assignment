﻿using System;
using System.Web.Script.Serialization;

namespace CommandLineProcessor
{
    public class JsonParser
    {

        private static JsonParser jsonParser = null;

        public static JsonParser getInstance() {

            if (jsonParser == null) {
                jsonParser = new JsonParser();
            }
            return jsonParser;

        }

        public T Parse<T>(string text,T t)
        {
            try
            {
                return new JavaScriptSerializer().Deserialize<T>(text);
            }
            catch (Exception)
            {
                Console.WriteLine("Unable to parse text");
                return default(T);
            }
        }
    }
}
