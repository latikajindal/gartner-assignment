﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace CommandLineProcessor
{
    public class SoftwareadviceWarehouseProcessor : IWarehouseProcessor
    {
        public void Process(string filePath)
        {
            try
            {
                string text = System.IO.File.ReadAllText(filePath);

                Products products = new JavaScriptSerializer().Deserialize<Products>(text);

                foreach (WarehouseItem item in products.products) {
                    Console.WriteLine("importing:"+item.ToString());
                }
            }

            catch (Exception ex)
            {
                Console.WriteLine("Exception while reading the file" + ex);
                return;
            }
        }
    }
}
