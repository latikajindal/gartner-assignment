﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace CommandLineProcessor
{
    public enum WarehouseProvider
    {
        capterra = ParserType.JSON,
        softwareadvice = ParserType.YAMl
    }

    public enum Command
    {
        import,
        exit
    }

    public class CommandProcessor
    {
        public void GetCommand()
        {
            string command;
            Console.WriteLine("Please Enter your Command and then press Enter(You can enter exit command to exit from system)");
            command = Console.ReadLine();

            if (command == "")
            {
                Console.WriteLine("Please enter a command");
                Console.ReadLine();
            }
            else
            {
                string[] split = command.Split(' ');

                if (split.Length != 3 && split.Length != 1)
                {
                    Console.WriteLine("Please enter valid command");
                    GetCommand();
                }
                else if (Enum.IsDefined(typeof(Command), split[0].ToLower()) && ((Command)Enum.Parse(typeof(Command), split[0].ToLower())).Equals(Command.exit) ) {
                    Console.WriteLine("Exiting Now!!");
                    return;
                }
                else
                {
                    if (Enum.IsDefined(typeof(Command), split[0].ToLower()))
                    {
                        if (Enum.IsDefined(typeof(WarehouseProvider), split[1].ToLower()))
                        {
                            IWarehouseProcessor warehouseProcessor = ProcessorFactory.getInstance((WarehouseProvider)Enum.Parse(typeof(WarehouseProvider), split[1].ToLower()));

                            warehouseProcessor.Process(split[2]);

                            GetCommand();
                        }
                        else
                        {
                            Console.WriteLine(split[1] + " is not a valid processor");
                            GetCommand();
                        }
                    }
                    else
                    {
                        Console.WriteLine(split[0] + " is not a valid command");
                        GetCommand();
                    }
                }
            }
        }
    }
}