﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandLineProcessor
{
    public interface IDbProvider<T>
    {
         Boolean Save(List<T> list);

         Boolean Save(T t);

        Boolean Delete(List<T> list);

        Boolean Delete(T t);

        Boolean Update(T t);
    }
}
