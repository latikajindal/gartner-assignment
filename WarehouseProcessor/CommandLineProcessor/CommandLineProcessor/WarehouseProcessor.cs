﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandLineProcessor
{
    public interface IWarehouseProcessor
    {
        void Process(string filePath);

    }
}
