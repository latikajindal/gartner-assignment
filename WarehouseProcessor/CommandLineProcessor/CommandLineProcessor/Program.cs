﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YamlDotNet.Serialization;
using System.Collections;
using System.IO;
using Newtonsoft.Json.Linq;

namespace CommandLineProcessor
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CommandProcessor cd = new CommandProcessor();
            cd.GetCommand();
        }
    }
}
