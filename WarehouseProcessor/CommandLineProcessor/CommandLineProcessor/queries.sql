﻿1. Select users whose id is either 3,2 or 4
- SELECT * FROM test.users where id in (2,3,4);

2. Count how many basic and premium listings each active user has
- 
SELECT 
    first_name, last_name, SUM(CASE WHEN listings.status = 2 THEN 1
             ELSE 0 END
 ) as basic, SUM(CASE WHEN listings.status = 3 THEN 1
             ELSE 0 END
 ) as premium
FROM
    users,
    listings
    WHERE
    users.id = listings.user_id
        
GROUP BY first_name , last_name

3. Show the same count as before but only if they have at least ONE premium listing
- 
select first_name,last_name,basic,premium from (SELECT 
    first_name, last_name, SUM(CASE WHEN listings.status = 2 THEN 1
             ELSE 0 END
 ) as basic, SUM(CASE WHEN listings.status = 3 THEN 1
             ELSE 0 END
 ) as premium
FROM
    users,
    listings
    WHERE
    users.id = listings.user_id
        
GROUP BY first_name , last_name ) temp where premium > 0
4. How much revenue has each active vendor made in 2013
- select first_name,last_name,currency,sum(price) from users,listings,clicks where users.id = listings.user_id and listings.id = clicks.listing_id and year(clicks.created) = '2013' group by first_name,last_name,currency


5. Insert a new click for listing id 3, at $4.00
- INSERT INTO `test`.`clicks` (`listing_id`, `price`, `currency`,`created`) VALUES ('3', '4.00', 'USD',curdate());
  SELECT LAST_INSERT_ID();

6. Show listings that have not received a click in 2013
- select name from (select name,count(clicks.id) count from listings left join clicks on listings.id = clicks.listing_id and year(clicks.created) = '2013' group by name) temp where temp.count = 0


7. For each year show number of listings clicked and number of vendors who owned these listings
- SELECT 
    YEAR(clicks.created) year,
    COUNT(listings.id),
    COUNT(DISTINCT user_id)
FROM
    users,
    listings,
    clicks
WHERE
    users.id = listings.user_id
        AND listings.id = clicks.listing_id
GROUP BY year


8. Return a comma separated string of listing names for all active vendors
- What is identification of active vendors.

